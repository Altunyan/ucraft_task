<div data-module="wallets-balance">
    <div class="wallets-balance">
        <h2 class="wallets-balance-header">Wallets Balance</h2>
        <div class="wallets-balance-info">
            @foreach($walletsBalance as $type => $balance)
                <h4>{{ ucfirst($type) }} : {{ $balance }}</h4>
            @endforeach
        </div>
    </div>
</div>