import Notification from '../utils/Notification';

class WalletCard {
    constructor() {
        this.addWalletBtn = $('.wallet-card-add-wallet-btn');
        this.selectWalletType = $('.wallet-card-select-type');

        this.addWalletBtn.click(() => {
            this.createWallet();
        });

        this.selectWalletType.change(() => {
            this.changeWalletType()
        });

        $(".delete-wallet").click((elem) => {
            this.addValuesToDeleteConfirmationModal($(elem.currentTarget))
        });

        $('.wallet-delete-confirmation-btn').click((elem) => {
            let id = $(elem.currentTarget).attr('data-id');

            this.deleteWallet(id);
        });

        $(".wallet-card-save-wallet-btn").click((elem) => {
            let id = $(elem.currentTarget).attr('data-id');

            this.updateWallet(id);
        });
    };

    /**
     * create wallet
     */
    createWallet() {
        let type = $('.wallet-card-select-type').val(),
            nameElem = $('.wallet-name');

        sendXhr('/createWallet', {
            type: type,
            name: nameElem.val()
        }).then((res) => {
            let response = res.responseText;

            if (response.status === 200) {
                nameElem.val('');
                Notification.show(response.message, 'success');
            } else {
                Notification.show(response.message);
            }
        });
    }

    /**
     * update wallet by id
     *
     * @param id
     */
    updateWallet(id) {
        let amount = $('.wallet-amount').val(),
            name = $('.wallet-name').val();

        sendXhr('/updateWallet', {
            id: id,
            amount: amount,
            name: name
        }).then((res) => {
            let response = res.responseText;

            if (response.status === 200) {
                Notification.show(response.message, 'success');
            } else {
                Notification.show(response.message);
            }
        });
    }

    /**
     * add id for delete confirmation modal button
     *
     * @param deleteBtn
     */
    addValuesToDeleteConfirmationModal(deleteBtn) {
        let id = deleteBtn.attr('data-id');

        $(".wallet-delete-confirmation-btn").attr('data-id', id);
        $("#wallet-delete-confirmation-modal").modal('show');
    }

    /**
     * delete wallet by id
     *
     * @param id
     */
    deleteWallet(id) {
        $("#wallet-delete-confirmation-modal").modal('hide');

        sendXhr('/deleteWallet', {id: id}).then((res) => {
            let response = res.responseText;

            if (response.status === 200) {
                $('#wallet' + id).remove();
                Notification.show(response.message, 'success');
            } else {
                Notification.show(response.message);
            }
        });
    }

    /**
     * change card sty;e by wallet type
     */
    changeWalletType() {
        let type = this.selectWalletType.find('option:selected').attr('data-type'),
            walletCardBlockType = $('.wallet-card-block > div');

        walletCardBlockType.removeClass();
        walletCardBlockType.addClass(type);
    }

    static _init() {
        if (document.querySelector('[data-module="wallet-card"]'))
            return new WalletCard();
    }
}

export default WalletCard._init;
