<div aria-live="polite" aria-atomic="true" style="position: relative;">
    <!-- Position it -->
    <div style="position: absolute; top: 0; right: 0; width: 350px;">
        <!-- Then put toasts within -->
        <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
            <div class="toast-header">
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading"></h4>
                </div>
            </div>
        </div>
    </div>
</div>
