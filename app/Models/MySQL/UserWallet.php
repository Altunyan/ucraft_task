<?php

namespace App\Models\MySQL;

use Illuminate\Database\Eloquent\Model;

class UserWallet extends Model
{
    protected $fillable = [
    	'user_id',
		'wallet_type_id',
		'name',
		'amount'
	];
}
