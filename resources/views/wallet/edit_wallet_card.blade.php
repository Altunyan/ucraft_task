<div data-module="wallet-card">
    <div class="wallet-card">
        <div class="wallet-card-block">
            <div class="{{ $wallet->wallet_type }}">
                <div class="wallet-card-block-header">
                    <h3 class="wallet-card-block-type"></h3>
                </div>

                <div class="wallet-card-block-wallet-name">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="form-control wallet-name" placeholder="Name" value="{{ $wallet->name }}">
                    </div>
                    <div class="form-group">
                        <label for="">Amount</label>
                        <input type="number" class="form-control wallet-amount" placeholder="Amount" value="{{ $wallet->amount }}">
                    </div>
                </div>

            </div>
        </div>
        <button type="button" class="btn btn-dark wallet-card-save-wallet-btn" data-id="{{ $wallet->id }}">save</button>
    </div>
</div>