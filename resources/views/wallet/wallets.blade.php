@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="text-center">
                @include('wallet.wallets_balance')
                <div class="wallets">
                    @foreach($wallets as $key => $wallet)
                        @include('wallet.wallet_card_block')
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @include('modals.wallet_delete_confirmation_modal')
@endsection
