import 'popper.js';
import 'bootstrap';
import './global';

//importing js from modules
import initWalletCard from './modules/wallet_card';

class Main {
    constructor() {
        //activating modules js
        initWalletCard();
    }
}

new Main();