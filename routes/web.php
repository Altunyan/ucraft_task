<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);


Route::get('/redirect', 'Auth\GoogleAuthController@redirect');
Route::get('/callback', 'Auth\GoogleAuthController@callback');

Route::group(['middleware' => ['verified']], function () {
	Route::group(['middleware' => ['walletAdded']], function () {
		Route::get('/home', 'HomeController@index')->name('home');

		//These routes are for WalletController
		Route::post('createWallet', 'WalletController@createWallet');
		Route::post('deleteWallet', 'WalletController@deleteWallet');
		Route::post('updateWallet', 'WalletController@updateWallet');
		Route::get('wallets', 'WalletController@wallets');
		Route::get('editWallet/{id}', 'WalletController@editWallet');
	});

	//These routes are for WalletController
	Route::get('addWallet', 'WalletController@addWallet')->name('addWallet	');
});