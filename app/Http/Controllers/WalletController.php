<?php

namespace App\Http\Controllers;

use App\Models\MySQL\UserWallet;
use App\Models\MySQL\WalletType;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WalletController extends Controller
{
	/**
	 * add wallet view action
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function addWallet()
	{
		$walletTypes = WalletType::all();

		return view('wallet.add_wallet', ['walletTypes' => $walletTypes]);
	}

	/**
	 * create wallet
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function createWallet(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required|string|max:255',
			'type' => 'required|integer',
		]);

		if ($validator->fails()) {
			return response(['message' => 'Please Feel All Fields'], Response::HTTP_BAD_REQUEST);
		}

		$response = ['status' => Response::HTTP_BAD_REQUEST];

		try {
			$name = $request->name;
			$user = Auth::user();
		    $walletType = WalletType::findOrFail($request->type);
			$wallet = UserWallet::where('user_id', $user->id)
				->where('wallet_type_id', $walletType->id)
				->where('name', $name)
				->first();

			if ($wallet) {
				$response['message'] = 'Wallet Already Exist';
			} else {
				UserWallet::create([
					'user_id' => $user->id,
					'wallet_type_id' => $walletType->id,
					'name' => $name
				]);

				$response = [
					'status' => Response::HTTP_OK,
					'message' => 'Wallet Created Successfully'
				];
			}
		} catch (ModelNotFoundException $e) {
			$response['message'] = 'Selected Wallet Type Not Exist';
		} catch (\Exception $e) {
			$response['message'] = 'Oops something went wrong!';
		}

		return response($response, $response['status']);
	}

	/**
	 * get all wallets by user
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function wallets()
	{
		$user = Auth::user();

		$walletsQuery = UserWallet::where('user_id', $user->id)
			->leftJoin('wallet_types', 'wallet_types.id', '=', 'user_wallets.wallet_type_id')
			->select('user_wallets.*', 'wallet_types.name as wallet_type');
		$walletsBalance = [
			'total' => $walletsQuery->sum('user_wallets.amount')
		];
		$wallets = $walletsQuery->get();
		$walletTypes = WalletType::all();

		foreach ($walletTypes as $walletType) {
			$info = $walletsQuery = UserWallet::where('user_id', $user->id)
				->where('wallet_type_id', $walletType->id)
				->sum('amount');

			$walletsBalance[$walletType->name] = $info;
		}

		return view('wallet.wallets', [
			'wallets' => $wallets,
			'walletTypes' => $walletTypes,
			'walletsBalance' => $walletsBalance
		]);
	}

	/**
	 * delete wallet by id
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function deleteWallet(Request $request)
	{
		try {
			$id = $request->id;
			$user = Auth::user();
			$wallet = UserWallet::where('user_id', $user->id)
				->where('id', $id)
				->firstOrFail();

			$wallet->delete();

			$response = [
				'status' => Response::HTTP_OK,
				'message' => 'Wallet Deleted Successfully'
			];
		} catch (ModelNotFoundException $e) {
			$response = [
				'status' => Response::HTTP_NOT_FOUND,
				'message' => 'Wallet Not Found'
			];
		} catch (\Exception $e) {
			$response = [
				'status' => Response::HTTP_BAD_REQUEST,
				'message' => 'Oops something went wrong!'
			];
		}

		return response($response, $response['status']);
	}

	/**
	 * get wallet edit page by wallet id
	 *
	 * @param $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function editWallet($id)
	{
		$user = Auth::user();
		$wallet = UserWallet::where('user_wallets.user_id', $user->id)
			->where('user_wallets.id', $id)
			->leftJoin('wallet_types', 'wallet_types.id', '=', 'user_wallets.wallet_type_id')
			->select('user_wallets.*', 'wallet_types.name as wallet_type')
			->first();

		if (!$wallet) {
			return redirect('wallets');
		}

		return view('wallet.edit_wallet', ['wallet' => $wallet]);
	}

	/**
	 * update wallet
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updateWallet(Request $request)
	{
		try {
			$id = $request->id;
			$user = Auth::user();
			$wallet = UserWallet::where('user_id', $user->id)
				->where('id', $id)
				->firstOrFail();

			$wallet->name = $request->name;
			$wallet->amount = $request->amount;

			$wallet->save();

			$response = [
				'status' => Response::HTTP_OK,
				'message' => 'Wallet Updated Successfully'
			];
		} catch (ModelNotFoundException $e) {
			$response = [
				'status' => Response::HTTP_NOT_FOUND,
				'message' => 'Wallet Not Found'
			];
		} catch (\Exception $e) {
			$response = [
				'status' => Response::HTTP_BAD_REQUEST,
				'message' => 'Oops something went wrong!'
			];
		}

		return response($response, $response['status']);
	}
}
