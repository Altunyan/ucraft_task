<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\MySQL\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class GoogleAuthController extends Controller
{
	public function redirect()
	{
		return Socialite::driver('google')->redirect();
	}


	public function callback()
	{
		try {
			$googleUser = Socialite::driver('google')->stateless()->user();
			$user = User::where('email', $googleUser->email)->first();
			$path = '/home';

			if(!$user) {
				$user = new User;

				$user->name = $googleUser->name;
				$user->email = $googleUser->email;
				$user->google_id = $googleUser->id;
				$user->password = md5(rand(1, 10000));

				$user->save();

				$path = '/addWallet';
			} else if (!$user->google_id) {
				$user->google_id = $googleUser->id;

				$user->save();
			}

			Auth::login($user);

			return redirect()->to($path);
		}
		catch (\Exception $e) {
			return 'Authentication Not Completed.';
		}
	}
}
