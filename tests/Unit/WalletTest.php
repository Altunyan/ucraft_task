<?php

namespace Tests\Unit;

use App\Http\Controllers\WalletController;
use App\Models\MySQL\User;
use App\Models\MySQL\UserWallet;
use App\Models\MySQL\WalletType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use PHPUnit\Framework\TestCase;

class WalletTest extends TestCase
{
	use RefreshDatabase, DatabaseMigrations;

	/**
	 * WalletTest constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$app = require __DIR__ . '/../../bootstrap/app.php';

		$app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();

		$this->addFirstUserToAuth();
	}

	/**
	 * send request
	 *
	 * @param $url
	 * @param $functionName
	 * @param string $method
	 * @param array $params
	 * @return array
	 */
	private function sendRequest($url, $method = 'GET', $params = [])
	{
		$request = Request::create($url, $method, $params ?? []);

		$controller = new WalletController();
		$response = $controller->$url($request);

		if ($method == 'GET') {
			return $response;
		} else {
			return [$response, json_decode($response->getContent())];
		}
	}

	/**
	 * get default parameters for wallet creating request
	 *
	 * @return array
	 */
	private function getDefaultParamsForWalletCreating()
	{
		$params = [
			'name' => md5(time()),
			'type' => WalletType::first()->id
		];

		return $params;
	}

	/**
	 * add default user to auth for tests
	 */
	private function addFirstUserToAuth()
	{
		$user = User::first();

		Auth::login($user);
	}

	/**
	 * test wallet adding view
	 *
	 * @return void
	 */
	public function testWalletAddView()
	{
		$response = $this->sendRequest('addWallet');

		$this->assertEquals('wallet.add_wallet', $response->name());
		$this->assertArrayHasKey('walletTypes', $response->getData());
	}

	/**
	 * test wallet creating without requiredParams
	 */
	public function testWalletCreatingWithoutRequiredParams()
	{
		list($response, $responseData) = $this->sendRequest('createWallet', 'POST');

		$this->assertEquals(400, $response->getStatusCode());
		$this->assertEquals('Please Feel All Fields', $responseData->message);
	}

	/**
	 * wallet creating with all required params.
	 *
	 * @return void
	 */
	public function testWalletCreating()
	{
		list($response, $responseData) = $this->sendRequest('createWallet', 'POST', $this->getDefaultParamsForWalletCreating());

		$this->assertEquals(200, $response->getStatusCode());
	}

	/**
	 * wallet creating with exist wallet params
	 */
	public function testWalletCreatingWithExistWalletParams()
	{
		$wallet = UserWallet::first();

		$params = [
			'name' => $wallet->name,
			'type' => $wallet->wallet_type_id,
		];

		list($response, $responseData) = $this->sendRequest('createWallet', 'POST', $params);

		$this->assertEquals(400, $response->getStatusCode());
		$this->assertEquals('Wallet Already Exist', $responseData->message);
	}

	/**
	 * test wallet creating with not exist wallet type
	 */
	public function testWalletCreatingWithNotExistWalletType()
	{
		$params = [
			'name' => md5(time()),
			'type' => rand(10000, 100000)
		];

		list($response, $responseData) = $this->sendRequest('createWallet', 'POST', $params);

		$this->assertEquals(400, $response->getStatusCode());
		$this->assertEquals('Selected Wallet Type Not Exist', $responseData->message);
	}

	/**
	 * get wallets
	 */
	public function testWallets()
	{
		$response = $this->sendRequest('wallets');
		$responseData = $response->getData();

		$this->assertEquals('wallet.wallets', $response->name());
		$this->assertArrayHasKey('wallets', $responseData);
		$this->assertArrayHasKey('walletTypes', $responseData);
		$this->assertArrayHasKey('walletsBalance', $responseData);
	}

	/**
	 * delete wallet with nor valid wallet id (not user wallet or not exist wallet)
	 */
	public function testDeleteWalletWithNotValidId()
	{
		list($response, $responseData) = $this->sendRequest('deleteWallet', 'POST', ['id' => time()]);

		$this->assertEquals(404, $response->getStatusCode());
	}

	/**
	 * delete wallet with valid params
	 */
	public function testDeleteWalletWithValidParams()
	{
		$wallet = UserWallet::where('user_id', Auth::user()->id)->first();
		
		list($response, $responseData) = $this->sendRequest('deleteWallet', 'POST', ['id' => $wallet->id]);

		$this->assertEquals(200, $response->getStatusCode());
	}

	/**
	 * update wallet with nor valid wallet id (not user wallet or not exist wallet)
	 */
	public function testUpdateWalletWithNotValidId()
	{
		list($response, $responseData) = $this->sendRequest('updateWallet', 'POST', ['id' => time()]);

		$this->assertEquals(404, $response->getStatusCode());
	}

	/**
	 * update wallet with valid params
	 */
	public function testUpdateWalletWithValidParams()
	{
		$wallet = UserWallet::where('user_id', Auth::user()->id)->first();

		list($response, $responseData) = $this->sendRequest('updateWallet', 'POST', [
			'id' => $wallet->id,
			'name' => md5(time()),
			'amount' => rand(1000, 10000)
		]);

		$this->assertEquals(200, $response->getStatusCode());
	}
}
