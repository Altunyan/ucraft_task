const xhrPromise = require('xhr-promise');

class Notification {

    // showing response notifications
    static show(message = 'Oops something went wrong!', type = 'error') {
        let toastElem = $('.toast'),
            toastBodyAlertElem = toastElem.find('.toast-body .alert');

        if (type === 'success') {
            toastBodyAlertElem.removeClass('alert-danger');
            toastBodyAlertElem.addClass('alert-success');
        } else {
            toastBodyAlertElem.removeClass('alert-success');
            toastBodyAlertElem.addClass('alert-danger');
        }

        $('.alert-heading').text(message);
        toastElem.removeClass('hidden');
        toastElem.toast('show');
    };
}

export default Notification;