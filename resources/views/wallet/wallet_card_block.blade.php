<div data-module="wallet-card" id="wallet{{ $wallet->id }}">
    <div class="wallet-card">
        <div class="wallet-card-block">
            <div class="{{ $wallet->wallet_type }}">
                <div class="wallet-card-block-header">
                    <h3 class="wallet-card-block-type"></h3>
                    <div class="wallet-card-block-header-actions">
                        <a href="{{ url('/editWallet/' . $wallet->id) }}"><span>&#9998;</span></a>
                        <button type="button" class="ml-2 mb-1 close delete-wallet" data-toggle="modal" data-id="{{ $wallet->id }}"
                                data-target="#delete-confirmation-modal">
                            <span>&times;</span>
                        </button>
                    </div>
                </div>
                <div class="wallet-card-block-info">
                    <div class="form-group wallet-card-block-info-wallet-name">
                        <div class="wallet-card-block-info-wallet-name-info info-block">
                            <h6>Name</h6>
                            <h5>{{ $wallet->name }}</h5>
                        </div>
                    </div>
                    <div class="form-group wallet-card-block-info-wallet-amount">
                        <div class="wallet-card-block-info-wallet-amount-info info-block">
                            <h6>Amount</h6>
                            <h5>{{ $wallet->amount }}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>