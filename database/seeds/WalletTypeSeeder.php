<?php

use App\Models\MySQL\WalletType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class WalletTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
        	'credit',
			'debit'
		];

		Schema::disableForeignKeyConstraints();
		DB::table('wallet_types')->truncate();

		foreach ($types as $type) {
			WalletType::create(['name' => $type]);
		}

		Schema::enableForeignKeyConstraints();
	}
}
