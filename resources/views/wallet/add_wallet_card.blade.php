<div data-module="wallet-card">
    <div class="wallet-card">
        <div class="form-group">
            <label for="select-wallet-card-type">Choose Wallet Type</label>
                <select class="form-control wallet-card-select-type" id="select-wallet-card-type">
                @foreach($walletTypes ?? '' as $walletType)
                    <option value="{{ $walletType['id'] }}" data-type={{ $walletType['name'] }}>{{ ucfirst($walletType['name']) }}</option>
                @endforeach
            </select>
        </div>
        <div class="wallet-card-block">
            <div class="{{ $walletTypes[0]['name'] }}">
                <div class="wallet-card-block-header">
                    <h3 class="wallet-card-block-type"></h3>
                </div>
                <div class="form-group wallet-card-block-wallet-name">
                    <input type="text" class="form-control wallet-name" placeholder="Name">
                </div>
            </div>
        </div>
        <button type="button" class="btn btn-dark wallet-card-add-wallet-btn">Add</button>
    </div>
</div>