# Documentation
- Create .env file in root directory for runing laravel.
- Copy all from .env.example and add to .env.
- Add db configs in .env.
- Add mail configs for verification(gmail)
- Generate app key for laravel.
    ```
       php artisan key:generate
    ```
- Create tables in db.
    ```
       php artisan migrate
    ```
- Install composer.
    ```
        composer install
    ```   
- Install npm.
    ```
        npm install
    ```
- After installation npm run webpack.
    ```
        npm run build
    ```
- Add wallet types to db.
    ```
       php artisan db:seeder --class=WalletTypeSeeder
    ```