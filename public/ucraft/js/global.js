import {$,jQuery} from 'jquery';

const xhrPromise = require('xhr-promise');

window.CSRFToken = document.getElementById('meta_csrf').content;
window.sendXhr = sendXhr;
window.sendXMLHttpRequest = sendXMLHttpRequest;
window.$ = $;
window.jQuery = jQuery;

function sendXhr(url, data) {
    return new xhrPromise().send({
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': CSRFToken,
            'Content-Type': 'application/json'
        },
        url: url,
        data: JSON.stringify(data),
    })
}

function sendXMLHttpRequest(url, formData, callback = false) {
    let request = new XMLHttpRequest();

    request.open('POST', url);
    request.setRequestHeader('X-CSRF-TOKEN', CSRFToken);
    request.send(formData);

    request.onload = (res) => {
        if (typeof callback === 'function') {
            callback(res.srcElement);
        }
    };
}