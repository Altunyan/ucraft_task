<div class="modal fade" id="wallet-delete-confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="wallet-delete-confirmation-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6>You Want You Sure Delete This Record?</h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger wallet-delete-confirmation-btn" data-id="">Delete</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>