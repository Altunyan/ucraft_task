<?php

namespace App\Http\Middleware;

use App\Models\MySQL\UserWallet;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class WalletAddedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$user = Auth::user();
    	$wallets = UserWallet::where('user_id', $user->id)->get();

    	if (!count($wallets)) {
			return redirect('addWallet');
		}

        return $next($request);
    }
}
